﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseScreen : MonoBehaviour {

    public AudioSource Bob;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PauseSound()
    {
        if(Bob.volume==1f)
        {
            Bob.volume = 0f;
        }

        else if (Bob.volume == 0f)
        {
            Bob.volume = 1f;
        }
    }
}
