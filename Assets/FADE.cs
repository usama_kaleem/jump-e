﻿using UnityEngine;
using System.Collections;

public class FADE : MonoBehaviour {

	// Use this for initialization
	void Start () {

        //StartCoroutine(FadeTo(0f, 1f));
        //iTween.FadeTo(gameObject, 0f, 1f);
        StartCoroutine(ChangeColorDown());

    }
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = GetComponent<SpriteRenderer>().color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            GetComponent<SpriteRenderer>().color = newColor;
            yield return null;
        }
    }

    IEnumerator ChangeColorDown()
    {
        iTween.FadeTo(gameObject, 0f, 1f);
        yield return new WaitForSeconds(1);
        StartCoroutine(ChangeColorUp());
    }

    IEnumerator ChangeColorUp()
    {
        iTween.FadeTo(gameObject, 1f, 1f);
        yield return new WaitForSeconds(1);
        StartCoroutine(ChangeColorDown());
    }
    /*iTween.MoveTo(gameObject, iTween.Hash("x", 6, "time", 4, "loopType", "pingPong"
         , "delay", .4, "easeType", "easeInOutQuad"));*/
}
