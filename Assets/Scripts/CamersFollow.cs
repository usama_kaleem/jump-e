﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CamersFollow : MonoBehaviour
{
    //FOR ENDLESS
    public LeafSpawn P1, P2;
    Vector3 PupilScale;

    Rigidbody2D BobRB;
    

    public Image ScoreImage; public Text ScoreText;
    public static float MoveSpeed = 0.15f;
    public NewJumpBob BobNJ;
    public GameObject Wires, PUPIL;

    public AudioSource HelpMe;
    public AudioClip HelpMeClip;

    public static float interpVelocity;
    public float minDistance;
    public float followDistance;

    GameObject target;
    public GameObject SaveMe, Bob;
    public Vector3 offset;
    public static float CinematicOffset = 0.015f;
    Vector3 targetPos;
    public Vector3 OrigTransform;
    public Transform Bachi;
    public GameObject TUTORIAL;


    // Use this for initialization
    void Start()
    {
        PupilScale = PUPIL.transform.localScale;
        TUTORIAL.SetActive(false);
        StartCoroutine(Example());

        BobRB = Bob.GetComponent<Rigidbody2D>();
        /*target = SaveMe;
        targetPos = transform.position;
        OrigTransform = transform.position;
        MoveSpeed = 0.15f;*/
    }

    // Update is called once per frame


    void Update()
    {
        /*if(Input.GetMouseButtonUp(0))
        {
            if(!StartScreen.SaveMeSeen)
            {
                StartScreen.SaveMeSeen = true;
                Wires.SetActive(true);
                ScoreText.enabled = ScoreImage.enabled = true;
                
            }

            if(StartScreen.tap)
            {
                //Bachi.parent = transform;
            }
        }
        if(StartScreen.SaveMeSeen)
        {
            target = Bob;
            CamersFollow.CinematicOffset = 0.075f;
            CamersFollow.MoveSpeed = 0.25f;
            //BobNJ.enabled = true;
        }*/
    }
    void FixedUpdate()
    {
        if (target)
        {
            Vector3 posNoZ = transform.position;
            posNoZ.z = target.transform.position.z;

            Vector3 targetDirection = (target.transform.position - posNoZ);

            interpVelocity = targetDirection.magnitude * 5f;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            transform.position = Vector3.Lerp(transform.position, targetPos + offset, MoveSpeed);
            transform.position = new Vector3(0, transform.position.y + CinematicOffset, transform.position.z);

        }
    }

    IEnumerator Example()
    {
        HelpMe.PlayOneShot(HelpMeClip, 0.4f);
        target = SaveMe;
        targetPos = transform.position;
        OrigTransform = transform.position;
        MoveSpeed = 0.15f;
        BobNJ.enabled = false;
        // print(Time.time);
        yield return new WaitForSeconds(3.7f);
        BobRB.gravityScale = 8f;
        StartCoroutine(EyeBlink());
        StartCoroutine(StartGame());
        //print(Time.time);
        target = Bob;
        Wires.SetActive(true);
        CamersFollow.CinematicOffset = 0.055f;
        CamersFollow.MoveSpeed = 0.25f;
    }

    IEnumerator StartGame()
    {

        yield return new WaitForSeconds(3.5f);
        SaveMe.SetActive(false); P1.enabled = true; P2.enabled = true;
        ScoreText.enabled = ScoreImage.enabled = true;
        BobNJ.enabled = true; TUTORIAL.SetActive(true);
    }

    IEnumerator EyeBlink()
    {
        yield return new WaitForSeconds(3.3f);
        iTween.ScaleTo(PUPIL, new Vector3(0.35f, 0.35f, 0.35f), 0.5f);
        yield return new WaitForSeconds(0.7f);
        iTween.ScaleTo(PUPIL, new Vector3(0.01f, 0.01f, 0.01f), 0.5f);
        yield return new WaitForSeconds(0.6f);
        iTween.ScaleTo(PUPIL, new Vector3(0.35f, 0.35f, 0.35f), 0.5f);
       /* yield return new WaitForSeconds(0.7f);
        iTween.ScaleTo(PUPIL, new Vector3(0.01f, 0.01f, 0.01f), 0.5f);
        yield return new WaitForSeconds(0.6f);
        iTween.ScaleTo(PUPIL, new Vector3(0.35f, 0.35f, 0.35f), 0.5f);*/
        //PUPIL.transform.localScale = new Vector3(0.6077774f, 0.6077774f, 0.6077774f);
    }
}