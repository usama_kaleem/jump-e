﻿using UnityEngine;
using System.Collections;

public class EndTutorialPlatform : MonoBehaviour {

    public EndTutorialPlatform ThisPlatform;
    public GameObject Light;
    public GameObject sparkoncollect;
    public GameObject i1,i2,i3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        /*if (PlayerPrefs.GetFloat("TutorialOK", 0f)==2f)
        {
            Destroy(Light.gameObject);
        }*/
	}

    void OnTriggerEnter2D(Collider2D A)
    {
        if(A.gameObject.tag=="Player" && PlayerPrefs.GetFloat("TutorialOK", 0f)==1f)
        {
            i1.SetActive(false);
            i2.SetActive(false);
            i3.SetActive(true);

            Debug.Log("Should talk about using slo mo");
            PlayerPrefs.SetFloat("TutorialOK", 2f);

           /* Destroy(Light.gameObject);
            GameObject K = Instantiate(sparkoncollect, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            K.transform.parent = gameObject.transform;
            Destroy(K, 1f);*/
        }
    }

    void OnTriggerExit2D (Collider2D A)
    {//
       // Debug.Log("Finish Tutorial");        
        //PlayerPrefs.SetFloat("TutorialOK", 0f);
        ThisPlatform.enabled = false;
        Tutorial.Step++;
        //Debug.Log("C: " + PlayerPrefs.GetFloat("TutorialOK", 0f));
    }
}