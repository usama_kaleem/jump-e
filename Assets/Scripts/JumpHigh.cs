﻿using UnityEngine;
using System.Collections;

public class JumpHigh : MonoBehaviour
{
    public Rigidbody2D Bob;
    public GameObject BobG;
    Collider2D BobsCollider;
    public float JumpHeight = 20f;

    public float timer;

    public static bool JHigh = false;

    public GameObject Camera;
    bool GOJUMP = false;

    // Use this for initialization
    void Start()
    {
        timer = 0f;
        BobsCollider = BobG.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GOJUMP)
            timer += Time.deltaTime;

        if (timer > 0.75f)
        {
            GOJUMP = false;
            BobsCollider.enabled = true;
            ResetTimer();
        }
    }

    void OnTriggerEnter2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player")
        {
            RespawnWire.IsJumper = false;

            CamersFollow.MoveSpeed = 0.40f;
            Bob.velocity = new Vector3(0f, JumpHeight, 0f);
            JHigh = true;
            Debug.Log("JHIGH = true");

            RespawnWire.ScoreA += 3f;
            RespawnWire.ScoreB += 3f;

           /* iTween.MoveAdd(Camera, new Vector3(0f, -50f, 0f), 0.8f);
            transform.position = new Vector3(transform.position.x, transform.position.y + 10f, transform.position.z);
            CamersFollow.MoveSpeed = 0.4f;
            */
            //Bob.AddForce(new Vector2 (0f, JumpHeight*2.5f));
            /*ConnectBob.Score += Random.Range(8, 12);*/ GOJUMP = true; A.enabled = false;
            //Stopper.transform.Translate(0f, 7f, 0f);
            ConnectBob.Score += 2f;
            ConnectBob.CanDisable = false;
            Debug.Log("CANNOT DISABLE");
            RespawnWire.IsJumper = false;
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D A)
    {
        if(A.gameObject.tag=="Player")
        {
            //RespawnWire.IsJumper = true;
            gameObject.SetActive(false);
        }
    }

    void ResetTimer()
    {
        timer = 0f;
    }
}
