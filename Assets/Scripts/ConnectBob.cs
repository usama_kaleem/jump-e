﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConnectBob : MonoBehaviour
{
    public static bool CanDisable = true;
    public bool BobIsOn = false;
    public GameObject Trail;
    TrailRenderer TrailR;

    public static float Score = -1f;
    public Text SCORE;

    public GameObject Bob;
    Rigidbody2D BobRB;
    //public Transform BobT;

    public Collider2D ColliderOfThisPlatform;
    public Collider2D TriggerOfThisPlatform;

    public bool Attach = false;

    // Use this for initialization
    void Start()
    {
        TrailR = Trail.GetComponent<TrailRenderer>();

        ColliderOfThisPlatform.enabled = true;
        TriggerOfThisPlatform.enabled = true;

        BobRB = Bob.GetComponent<Rigidbody2D>();
        Score =0f;
        BobIsOn = false;
    }

    // Update is called once per frame
    void Update()
    {

        //MAKES BOB FALL DOWN ON BLAST
        /*if (NewJumpBob.BlastOnce)
        {
            ColliderOfThisPlatform.enabled = false;
            TriggerOfThisPlatform.enabled = false;
        }*/

        if (!CanDisable/* && BobRB.velocity.y > -5f && BobRB.velocity.y < 5f*/)
        {
            if (Input.GetMouseButtonDown(0))
            {
                CanDisable = true;
               // Debug.Log("CAN DISABLE");
            }
        }
        //DoConnect();
        DisableShiz();
        UpdateScore();
    }

    void OnTriggerStay2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player")
        {

            //Vector3.MoveTowards(A.transform.position, transform.position, 0f);
            //Debug.Log("Attach");
            //Attach = true;
            //BobRB.velocity = Vector3.zero;

            if (Input.GetMouseButtonUp(0) && !EndGame.isdead/*&&Score>0f*/)
            {
                ColliderOfThisPlatform.enabled = false;
                TriggerOfThisPlatform.enabled = false;
            }
        }
    }

    void OnTriggerExit2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player")
        {
            Debug.Log("Should Disable Colls");
        }
    }

    void OnTriggerEnter2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player")
        {

            /*if (!NewJumpBob.ResetSpeedOnce)
            {
                SpeedController.PlatformXSpeed = NewJumpBob.OriginalXSpeed;
                NewJumpBob.ResetSpeedOnce = true;
                Debug.Log("RESET SPEED");
            }
           NewJumpBob.SlowOnce = false;*/

            if (!CanDisable)
            {
                //ConnectBob.Score += Random.Range(8, 12);
            }

            iTween.PunchPosition(gameObject, new Vector3(0f, -0.15f, 0f), 1f);
            //Time.timeScale = 1.5f;

            BobIsOn = true;

            Transform TBob = Bob.GetComponent<Transform>();
            TBob.parent = this.gameObject.transform;

            Score++;
            TrailR.enabled = false;
            //Vector3.MoveTowards(A.transform.position, transform.position, 0f);
            CamersFollow.MoveSpeed = 0.15f;
            //Debug.Log("Attach");
            Attach = true;
            BobRB.velocity = Vector3.zero;

            //NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed = SpeedController.PlatformXSpeed * 1.02f;
            NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed = NewJumpBob.SpeedAtJumpUp * 1.02f;

            if (!NewJumpBob.CanJump && NewJumpBob.SlowOnce)
            {
                NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed *= 5f;
            }

            //NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed = Mathf.Log(Mathf.Sin(Score) * 0.5f + Score + 2, 2f);
            //NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed = SpeedController.PlatformXSpeed * 1.014f;
            //NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed = SpeedController.PlatformXSpeed + 1.05f;
            //NewJumpBob.OriginalXSpeed = SpeedController.PlatformXSpeed = Mathf.Log(SpeedController.ScoreS, 2.5f);
            //Debug.Log("SPEED : " + NewJumpBob.OriginalXSpeed);

            NewJumpBob.SpeedX = SpeedController.PlatformXSpeed;

        }
    }



    void DoConnect()
    {
        if (Attach)
        {
            Bob.transform.parent = transform;
        }

        if (!Attach)
        {
            Bob.transform.parent = null;
        }
    }

    void UpdateScore()
    {
        SCORE.text = Score.ToString();
    }

    void DisableShiz()
    {
        Transform T = Bob.GetComponent<Transform>();
        if (T.position.y > transform.position.y + 3f && CanDisable)
        {
            //Debug.Log("Shiz Disabled");
            ColliderOfThisPlatform.enabled = false;
            TriggerOfThisPlatform.enabled = false;
        }
    }
}
