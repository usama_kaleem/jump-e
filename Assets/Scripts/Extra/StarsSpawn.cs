﻿using UnityEngine;
using System.Collections;

public class StarsSpawn : MonoBehaviour {

    public Camera Cam;

    public Vector3 RLimit, LLimit;

	// Use this for initialization
	void Start () {
        RLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
	}
	
	// Update is called once per frame
	void Update () {
        RLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
        //Debug.Log("AAA");
        if(transform.position.y<LLimit.y-5f)
        {
            
            RespawnIt();
        }
	}

    void RespawnIt()
    {
        transform.position = new Vector3(transform.position.x, RLimit.y + 5f, transform.position.z);
        //Debug.Log("Should Respawn");
    }
}
