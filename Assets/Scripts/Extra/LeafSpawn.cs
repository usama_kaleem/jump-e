﻿using UnityEngine;
using System.Collections;

public class LeafSpawn : MonoBehaviour {

	Camera Cam;

    public Vector3 RLimit, LLimit;

    public float RespawnHeightA, RespawnHeightB;

	// Use this for initialization
    void Start()
    {

        //FIND CAMERA

        Cam = GameObject.FindObjectOfType<Camera>();
        RLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
    }
	
	// Update is called once per frame
	void Update () {

        RLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        if (transform.position.y < LLimit.y-20f)
        {
            RespawnIt();
        }
	}

    void RespawnIt()
    {
        Random.seed = System.DateTime.Now.Millisecond;
        transform.position = new Vector3(transform.position.x, RLimit.y+Random.Range(RespawnHeightA, RespawnHeightB), transform.position.z);
        //Debug.Log("Should Respawn");
    }
}
