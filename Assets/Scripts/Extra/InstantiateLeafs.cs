﻿using UnityEngine;
using System.Collections;

public class InstantiateLeafs : MonoBehaviour {

    public bool OnRight;

    public Camera Cam;

    public Vector3 ULimit, DLimit;

    // Use this for initialization
    void Start()
    {
        ULimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        DLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        if (OnRight)
        {
            Random.seed = System.DateTime.Now.Millisecond;
            transform.position = new Vector3(ULimit.x, transform.position.y/*5f+Random.Range(ULimit.y, DLimit.y)*/, -5f);
        }

        else if (!OnRight)
        {
            Random.seed = System.DateTime.Now.Millisecond;
            transform.position = new Vector3(DLimit.x, transform.position.y/*5f+Random.Range(ULimit.y, DLimit.y)*/, -5f);
        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
