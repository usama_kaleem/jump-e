﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Charger : MonoBehaviour {

    public Slider Mana;

    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;

	// Use this for initialization
	void Start () {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
	}
	
	// Update is called once per frame
	void Update () {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
	}

    void OnTriggerEnter2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player")
        {
            Mana.value += 35f;

            if (Mana.value > 100f)
            {
                Mana.value = 100f;
            }

            transform.position = new Vector3(Random.Range(LeftLimit.x, RightLimit.x), RightLimit.y + Random.Range(9f, 12f));
        }
    }
}
