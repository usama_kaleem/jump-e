﻿using UnityEngine;
using System.Collections;

public class FirstPoleSpawn : MonoBehaviour {

    LeafSpawn ThisObject;

	// Use this for initialization
	void Start () {
        ThisObject = GetComponent<LeafSpawn>();
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EndGame.isdead && StartScreen.tap && StartScreen.SaveMeSeen)
        {
            //Debug.Log("Start Poles");
            ThisObject.enabled = true;
        }
    }
}
