﻿using UnityEngine;
using System.Collections;

public class SpeedController : MonoBehaviour
{

    public static float PlatformXSpeed;
    public static float PlatformYSpeed;

    public float SlowMoTime = 7f;

    public float SlowMoSpeedX = 0.5f;
    public float SlowMoSpeedY = 0.0f;


    void Awake()
    {
        PlatformXSpeed = 1.9f;
        PlatformYSpeed = 0f;
    }

    public void IncSpeed(float factor, float limit)
    {
        if (PlatformXSpeed < limit)
            PlatformXSpeed *= factor;
    }
}
