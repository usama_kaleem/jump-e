﻿using UnityEngine;
using System.Collections;

public class GoThrough : MonoBehaviour {

    public Vector3 Vel;
    Rigidbody2D RB;
    public Collider2D Col;


	// Use this for initialization
	void Start () {
        RB = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        Vel = RB.velocity;
        Col.enabled = true;


        if(Vel.y>0.5f && RestartLevelBeta.GoThrough)
        {
            Col.enabled = false;
        }
	}
}
