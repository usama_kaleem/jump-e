﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewJumpBob : MonoBehaviour
{
    public static float SpeedAtJumpUp = 1.4f;

    public Animator Rain;
    public AudioSource BobSpeaker;
    public AudioClip BlastScream, DieScream;
    public GameObject i1, i2, i3;

    public GameObject pupil;
    Vector3 pupilscale;
    public static bool Zoomin = false;

    public GameObject W1, W2, W3, W4, W5;
    MovePlatforms mW1, mW2, mW3, mW4, mW5;

    AudioSource BobA;
    float PitchBob;
    public GameObject Spark, SparkHere;

    public GameObject Blast, Explosion;
    public static bool BlastOnce;

    bool T = false;
    float t = 0f;

    public GameObject D;
    Renderer R;

    Color CR, C;// = new Color(R.material.color.r + 20f, R.material.color.g, R.material.color.b);

    public GameObject Trail;
    TrailRenderer TrailR;
    public Camera Cam;

    Rigidbody2D Bob;
    //public Collider2D Col;

    static float ManaReduceSpeed = 20f;

    public float Jump = 15f;
    public Vector3 JumpV;

    public Slider Mana;
    public GameObject ManaGO;

    public static float OriginalXSpeed;
    public static float SpeedX;

    public static bool SlowMo = false;
    public static bool SlowOnce = false;
    public static bool StartSloTimer;
    public static bool ResetSpeedOnce;

    public static bool StartSlowMo; //Because its being used in Tiles Shake
    public static bool CanJump;

    public static bool MouseDown;
    float timer;

    public Text Instructions;
    public Image InstructionsImage;

    public Vector3 LeftL, RightL;

    public EndGame CameraHere;

    public Animator Anim;
    public LeafSpawn Pole1;

    public float SpeedK = 1f;

    Color A, B;         //Pupil Colors
    // Use this for initialization
    void Start()
    {
        SpeedAtJumpUp = SpeedController.PlatformXSpeed;
        SpeedX = SpeedController.PlatformXSpeed;

        Time.timeScale = SpeedK;

        pupilscale = pupil.transform.localScale;
        A = pupil.GetComponent<SpriteRenderer>().color;
        B = A;// + new Color(100f, -50f, -50f);


        mW1 = W1.GetComponent<MovePlatforms>();
        mW2 = W2.GetComponent<MovePlatforms>();
        mW3 = W3.GetComponent<MovePlatforms>();
        mW4 = W4.GetComponent<MovePlatforms>();
        mW5 = W5.GetComponent<MovePlatforms>();

        //Anim = GetComponent<Animator>();

        BobA = GetComponent<AudioSource>();

        PitchBob = BobA.pitch;

        BobA.enabled = StartScreen.PLAYSOUND;

        BlastOnce = false;

        R = D.GetComponent<Renderer>();
        CR = new Color(R.material.color.r + 20f, R.material.color.g, R.material.color.b);
        C = new Color(R.material.color.r, R.material.color.g, R.material.color.b);
        //ChangeInstrAlpha();
        TrailR = Trail.GetComponent<TrailRenderer>();
        TrailR.enabled = false;
        SlowMo = false;
        SlowOnce = false;
        MouseDown = false;
        StartSloTimer = false;
        ResetSpeedOnce = false;
        timer = 0f;
        StartSlowMo = false;

        CanJump = true;

        Bob = GetComponent<Rigidbody2D>();
        JumpV = new Vector3(0f, Jump);

        OriginalXSpeed = SpeedController.PlatformXSpeed;
        Instructions.enabled = true;
        InstructionsImage.enabled = true;

        LeftL = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
        RightL = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));


        Mana.value = 30f;
        //Mana.enabled = false;
        //Debug.Log("Disable Mana" + Mana.enabled);

        ManaGO.SetActive(false);
        //iTween.LoopType.loop()

    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log("TIMESCALE: " + Time.timeScale);
        Jumpi();
        //Debug.Log("OKA");
        //Debug.Log("CANJ: " + CanJump);
        LeftL = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
        RightL = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));

        //Mana.value -= 0.5f * Time.deltaTime;



        /*if (MouseDown && Input.GetMouseButtonUp(0))
        {
            TrailR.enabled = true;
            transform.parent = null;
            MouseDown = false;
            StartSloTimer = false; timer = 0f;
            if (CanJump) { Bob.isKinematic = false; Bob.velocity = new Vector2(0f, 0f); Bob.velocity = JumpV; CanJump = false; }
        }*/
        //Debug.Log("isDead: " + EndGame.isdead + ", StartScreen.tap: " + StartScreen.tap + ", StartScreen.SaveMeSeen: " + StartScreen.SaveMeSeen);
        //Debug.Log("TutoiOK: " + PlayerPrefs.GetFloat("TutorialOK", 0f));
        if (Input.GetMouseButtonDown(0) && !MouseDown && !EndGame.isdead/* && StartScreen.tap/* && StartScreen.SaveMeSeen*/)
        {
            SpeedAtJumpUp = SpeedController.PlatformXSpeed;
            //Debug.Log("A");
            //CamersFollow.MoveSpeed = 0.15f;
            //Mana.enabled = true;
            MouseDown = true;
            StartSloTimer = true;
            Instructions.enabled = false;
            InstructionsImage.enabled = false;

            if (Pole1)
            {
                Pole1.enabled = true;
            }

            if (!CameraHere.enabled)
            {
                CameraHere.enabled = true;
            }

        }

        if (MouseDown && Input.GetMouseButtonUp(0))
        {

            //Debug.Log("GetmousebuttonUp");
            Rain.speed = 1f;

            //BobA.pitch = PitchBob;
            //Debug.Log("B");
            Mana.enabled = false;
            ManaGO.SetActive(false);
            //Debug.Log("Disable Mana" + Mana.enabled);
            Mana.value = 30f;
            gameObject.transform.parent = null;
            TrailR.enabled = true;
            transform.parent = null;
            MouseDown = false;
            StartSloTimer = false; timer = 0f;
            CamersFollow.MoveSpeed = 0.15f;
            if (CanJump && !EndGame.isdead) { Bob.isKinematic = false; Bob.velocity = new Vector2(0f, 0f); Bob.velocity = JumpV; CanJump = false; Anim.SetBool("Jump", true); Anim.SetBool("SlowMo", false); }



            if (!StartScreen.tap)
            {
                StartScreen.tap = true;
            }

            if (Zoomin)
            { iTween.ScaleBy(pupil, new Vector3((1 / 2.7f), (1 / 2.7f), (1 / 2.7f)), 1f);/* iTween.ScaleBy(pupil, new Vector3(1.7f, 1.7f, 1.7f), 1f); iTween.MoveBy(pupil, new Vector3(0f, 0.05f, 0f), 1f);*//* pupil.transform.localScale = pupilscale; */Zoomin = false; }
        }


        //CheckCols();

        //PushBack();
    }

    void FixedUpdate()
    {

        /*if (Input.GetMouseButtonDown(0) && !MouseDown && !EndGame.isdead)
        {

            MouseDown = true;
            StartSloTimer = true;
            Instructions.enabled = false;
            InstructionsImage.enabled = false;
        }

        if (MouseDown && Input.GetMouseButtonUp(0) && !EndGame.isdead)
        {
            Mana.value = 30f;
            gameObject.transform.parent = null;
            TrailR.enabled = true;
            transform.parent = null;
            MouseDown = false;
            StartSloTimer = false; timer = 0f;
            if (CanJump) { Bob.isKinematic = false; Bob.velocity = new Vector2(0f, 0f); Bob.velocity = JumpV; CanJump = false; }
        }*/

        StartTimer();
        CheckTimer();
        GoSlow();
    }

    void StartTimer()
    {
        if (StartSloTimer)
        {
            timer += Time.deltaTime;
        }
    }

    void CheckTimer()
    {
        if (timer > 0.2f)
        {
            if (PlayerPrefs.GetFloat("TutorialOK", 0f) > 0f)
            {
                StartSlowMo = true;

                ManaGO.SetActive(true);
                //Mana.enabled = true;
                //Debug.Log("Enable Mana" + Mana.enabled);
            }
        }
        else if (timer < 0.1f)
        {
            StartSlowMo = false;
        }
    }

    void GoSlow()
    {
        if (StartSlowMo && !SlowOnce && !EndGame.isdead)
        {
            //R.material.color = Color.Lerp(R.material.color, CR, 10f);
            //SpeedController.PlatformXSpeed = 0.38f;//0.20f * SpeedController.PlatformXSpeed;
            SpeedController.PlatformXSpeed = 0.2f * SpeedX;
            SlowOnce = true;
            ResetSpeedOnce = false;
            Anim.SetBool("SlowMo", true);

            Rain.speed -= 0.07f;

            if (i2.activeSelf && !i1.activeSelf)
            {
                i2.SetActive(false);
                //i3.SetActive(true);
                PlayerPrefs.SetFloat("TutorialOK", 2f);
            }

            if (!Zoomin)
            { iTween.ScaleBy(pupil, new Vector3(2.7f, 2.7f, 2.7f), 1f);/*iTween.ScaleBy(pupil, new Vector3((1 / 1.7f), (1 / 1.7f), (1 / 1.7f)), 1f);*/ Zoomin = true; }
        }

        else if (StartSlowMo && SlowOnce && !EndGame.isdead)
        {
            if (Mana.value > 0)
            {
                Mana.value -= ManaReduceSpeed * Time.deltaTime;

                //BobA.pitch = 1.15f;

                //Color
                R.material.color = new Color(R.material.color.r + 0.0080f, R.material.color.g - 0.001f, R.material.color.b - 0.001f);
                pupil.GetComponent<SpriteRenderer>().color = new Color(pupil.GetComponent<SpriteRenderer>().color.r + 0.007f, pupil.GetComponent<SpriteRenderer>().color.g - 0.01f, pupil.GetComponent<SpriteRenderer>().color.b - 0.01f);

                //Debug.Log("COLOR R: " + R.material.color.r);
                //R.material.color = Color.Lerp(R.material.color, CR, 1000f);

                //Debug.Log("COLOR");
            }
            else if (Mana.value <= 0 && !BlastOnce)
            {
                Debug.Log("Blast");

                if (BlastScream)
                    BobSpeaker.PlayOneShot(BlastScream, 0.8f);

                /*
                NewJumpBob.MouseDown = false;
                NewJumpBob.StartSlowMo = false;
                NewJumpBob.SlowOnce = false;
                */

                GameObject B = Instantiate(Blast, transform.position, transform.rotation) as GameObject;
                Destroy(B, 0.8f);

                BlastOnce = true;

                //if(Input.GetMouseButtonUp(0))
                EndGame.isdead = true;
                //REMOVE LATERON
                //Mana.value += 50f;
            }
        }

        else if (!StartSlowMo && !EndGame.isdead)
        {
            pupil.GetComponent<SpriteRenderer>().color = B;
            pupil.transform.localScale = pupilscale;
            if (!ResetSpeedOnce)
            {
                if (ConnectBob.Score < 1f)
                    //SpeedController.PlatformXSpeed = OriginalXSpeed;
                    ResetSpeedOnce = true;
            }
            SlowOnce = false;

            if (R.material.color.r > C.r)
            {
                //Debug.Log("Reset Color");
                R.material.color = new Color(R.material.color.r - 0.020f, R.material.color.g + 0.0025f, R.material.color.b + 0.0025f);
            }
        }
    }


    void OnCollisionEnter2D(Collision2D A)
    {
        // Debug.Log("StartSlomo " + StartSlowMo + ", SlowOnce " + SlowOnce);
        if (ConnectBob.Score > 0f && !StartSlowMo && !EndGame.isdead)
        {
            /*if (R.material.color.r > C.r)
            {
                //Debug.Log("Reset Color");
                R.material.color = new Color(R.material.color.r - 0.060f, R.material.color.g + 0.01f, R.material.color.b + 0.01f);
            }*/

          // R.material.color = C;
            SpeedController.PlatformXSpeed = SpeedX;//= OriginalXSpeed;// OriginalXSpeed;//SpeedController.PlatformXSpeed * 5f;
            Debug.Log("SpeedX : " + SpeedX);
            SlowOnce = false;
        }

        if (!i2.activeSelf && PlayerPrefs.GetFloat("TutorialOK", 0f) == 2f)
        {
            //   Debug.Log("i3 en");
            //i3.SetActive(true);
        }

        if (ConnectBob.Score == 0f)
        {
            EndGame.isdead = false;
        }

        Anim.SetBool("Jump", false);
        if (A.gameObject.tag == "Platform")
        {

            JumpHigh.JHigh = false;
            CanJump = true;
            //Debug.Log("Can");



            GameObject S = Instantiate(Spark, SparkHere.transform.position, SparkHere.transform.rotation) as GameObject;
            S.transform.parent = A.transform;
            Destroy(S, 2f);
        }

        else/* if (EndGame.isdead)*/
        {
            //CanJump = true;
            //Debug.Log("BOOM");
            GameObject C = Instantiate(Explosion, transform.position, transform.rotation) as GameObject;
            Destroy(C, 0.9f);

            EndGame.isdead = true;

            if (DieScream)
                BobSpeaker.PlayOneShot(DieScream, 0.8f);
        }
    }

    void OnCollisionExit2D(Collision2D A)
    {
        //CanJump = false;              ////////////////////////////////////////////////////////////////////CHANGED HERE
        //Debug.Log("Cant");
    }

    /*void PushBack()
    {
        if (transform.position.x > RightL.x - 0.2f)
        {
            Debug.Log("Should Push Left");
            //Bob.velocity = new Vector3(-5f, 0f, 0f);
            Bob.AddForce(new Vector2(-50f,0f));
        }

        if (transform.position.x < LeftL.x + 0.2f)
        {
            Debug.Log("Should Push Right");
            //Bob.velocity = new Vector3(-5f, 0f, 0f);
            Bob.AddForce(new Vector2(50f,0f));
        }
    }*/

    /*void CheckCols()
    {
        if(Bob.velocity.y<0.5f)
        {
            Col.enabled = true;
            Debug.Log("Col Enabled");
        }

        else if (Bob.velocity.y > 0.5f)
        {
            Col.enabled = false;
            Debug.Log("Col Disabled");
        }
    }*/

    public void CanJ()
    {
        CanJump = true;
        // Debug.Log("SETCANJ : " + CanJump);
    }

    public void Jumpi()
    {
        if (Bob.velocity.y < -0f && JumpHigh.JHigh)
        {
            //Debug.Log("Jump Setting");
            mW1.enabled = mW2.enabled = mW3.enabled = mW4.enabled = mW5.enabled = false;

            if (Mathf.Abs(Bob.gameObject.GetComponent<Transform>().position.y - W1.transform.position.y) < 0.5f)
                W1.transform.position = new Vector3(transform.position.x, W1.transform.position.y, W1.transform.position.z);

            if (Mathf.Abs(Bob.gameObject.GetComponent<Transform>().position.y - W2.transform.position.y) < 0.5f)
                W2.transform.position = new Vector3(transform.position.x, W2.transform.position.y, W2.transform.position.z);

            if (Mathf.Abs(Bob.gameObject.GetComponent<Transform>().position.y - W3.transform.position.y) < 0.5f)
                W3.transform.position = new Vector3(transform.position.x, W3.transform.position.y, W3.transform.position.z);

            if (Mathf.Abs(Bob.gameObject.GetComponent<Transform>().position.y - W4.transform.position.y) < 0.5f)
                W4.transform.position = new Vector3(transform.position.x, W4.transform.position.y, W4.transform.position.z);

            if (Mathf.Abs(Bob.gameObject.GetComponent<Transform>().position.y - W5.transform.position.y) < 0.5f)
                W5.transform.position = new Vector3(transform.position.x, W5.transform.position.y, W5.transform.position.z);
        }

        else//if (JumpHigh.JHigh)
        {
            //  Debug.Log("Jump Setting");
            mW1.enabled = mW2.enabled = mW3.enabled = mW4.enabled = mW5.enabled = true;

            //W1.transform.position = new Vector3(transform.position.x, W1.transform.position.y, W1.transform.position.z);
            //W2.transform.position = new Vector3(transform.position.x, W2.transform.position.y, W2.transform.position.z);
            //W3.transform.position = new Vector3(transform.position.x, W3.transform.position.y, W3.transform.position.z);
            // W4.transform.position = new Vector3(transform.position.x, W4.transform.position.y, W4.transform.position.z);
            //W5.transform.position = new Vector3(transform.position.x, W5.transform.position.y, W5.transform.position.z);
        }
    }

}
