﻿using UnityEngine;
using System.Collections;

public class BobInScreen : MonoBehaviour {

    public Camera Cam;

    public Vector3 R, L;

    //public bool isRight;

    // Use this for initialization
    void Start()
    {
        R = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        L = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
	}
	
	// Update is called once per frame
	void Update () {
	    
        if(transform.position.x>=R.x)
        {
            transform.position = new Vector3(R.x, transform.position.y, transform.position.z);
        }

        if (transform.position.x <= L.x)
        {
            transform.position = new Vector3(L.x, transform.position.y, transform.position.z);
        }

	}
}
