﻿using UnityEngine;
using System.Collections;

public class SetBoundaries : MonoBehaviour {

    public Transform Bob;
    public Camera Cam;

    public Vector3 R, L;

    public bool isRight;

	// Use this for initialization
	void Start () {
        R = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        L = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        if (isRight)
            transform.position = new Vector3(R.x, Bob.position.y);

        else if (!isRight)
            transform.position = new Vector3(L.x, Bob.position.y);
	}
	
	// Update is called once per frame
	void Update () {
        R = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        L = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        if (isRight)
            transform.position = new Vector3(R.x, Bob.position.y);

        else if (!isRight)
            transform.position = new Vector3(L.x, Bob.position.y);
	}
}
