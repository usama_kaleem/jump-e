﻿using UnityEngine;
using System.Collections;

public class THUNDERCLAP : MonoBehaviour {

    public GameObject Quad;
    public float timer = 0f, claptime= 2f;
    Color OriginalColor;
    bool clapping = false;
    public AudioSource ClapSounder;
    public AudioClip ThunderClap;
    bool Clapped = false;

	// Use this for initialization
	void Start () {
        //timer = Random.Range(8f, 12f);
        ClapSounder = GetComponent<AudioSource>();
        OriginalColor = Quad.GetComponent<Renderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        Clap();
    }

    void FixedUpdate()
    {
        
    }

    void Clap()
    {
        //OriginalColor = GetComponent<Renderer>().material.color;
        clapping = true;
        if (timer >= claptime)
        {
            if (!Clapped)
            { ClapSounder.PlayOneShot(ThunderClap, 0.9f); Clapped = true; }
            Quad.GetComponent<Renderer>().material.color = new Color(200f, 200f, 240f);// StartCoroutine(CLAP());
        }

        if (timer >= claptime+0.05f)
        {
            Quad.GetComponent<Renderer>().material.color = OriginalColor;
        }

        if (timer >= claptime+1.3f)
            Quad.GetComponent<Renderer>().material.color = new Color(1700f, 170f, 190f);

        if (timer >= claptime+1.5f)
        {
            Quad.GetComponent<Renderer>().material.color = OriginalColor;
        }

        if (timer >= claptime+1.7f)
            Quad.GetComponent<Renderer>().material.color = new Color(230f, 230f, 250f);

        if (timer >= claptime+1.9f)
        {
            Clapped = false;
            Quad.GetComponent<Renderer>().material.color = OriginalColor;
            claptime = timer + Random.Range(10f, 30f);
            //Debug.Log("NEXT CLAP: " + claptime);
        }
    }
}
