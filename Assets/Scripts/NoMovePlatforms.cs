﻿using UnityEngine;
using System.Collections;

public class NoMovePlatforms : MonoBehaviour
{

    public NoMovePlatforms ThisPlatformAlso;
    public MovePlatforms ThisPlatform;
    //public GameObject Light;
    public Transform Bob;
    public GameObject i1, i2, i3;

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetFloat("TutorialOK", 0f) == 1f)
        {
            ThisPlatformAlso.enabled = false;
        }

        if (Tutorial.Step == 0f)
        {
            //Tap to Jump here
            ThisPlatform.enabled = false;
            // Debug.Log("Not Moving");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if(Input.GetMouseButtonUp(0))
        /*if(ConnectBob.Score==0f)
        {
            ThisPlatformAlso.enabled = true;
        }*/

        if (/*Tutorial.Step == 0f&& */PlayerPrefs.GetFloat("TutorialOK", 0f) == 0f)
        {
            //Tap to Jump here
            ThisPlatform.enabled = false;
            //Debug.Log("Not Moving");
            transform.position = new Vector3(Bob.transform.position.x, transform.position.y, transform.position.z);
        }

        /*if (PlayerPrefs.GetFloat("TutorialOK", 0f) == 1f)
        {
            //Destroy(Light.gameObject);
        }*/
    }

    void OnTriggerEnter2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player")
        {
            if (PlayerPrefs.GetFloat("TutorialOK", 0f) == 0f)
            {
                Debug.Log("Should talk about grabbing slo mo");
                if (i1)
                    i1.SetActive(false);
                if (i2)
                    i2.SetActive(true);
                if (i3)
                    i3.SetActive(false);

                PlayerPrefs.SetFloat("TutorialOK", 2f);
            }
        }   
    }

    /* void OnTriggerExit2D(Collider2D A)
     {
         if (A.gameObject.tag == "Player")
         {
             if (Tutorial.Step == 0f)
             {
                 Debug.Log("Moving");
                 ThisPlatform.enabled = true;
                 Tutorial.Step++;
                 ThisPlatformAlso.enabled = false;
                 Debug.Log("B: " + PlayerPrefs.GetFloat("TutorialOK", 0f));
             }
         }
     }*/
}
