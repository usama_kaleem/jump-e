﻿using UnityEngine;
using System.Collections;

public class MovePlatforms2 : MonoBehaviour
{

    public float XSpeed;
    public float YSpeed;
    public bool MovingRight;

    public bool Fall = false;

    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;

    public static float previousX;

    void Awake()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
        previousX = LeftLimit.x;
    }

    // Use this for initialization
    void Start()
    {
        XSpeed = SpeedController.PlatformXSpeed;
        YSpeed = SpeedController.PlatformYSpeed;
        //MovingRight = true;           //Default

    }

    // Update is called once per frame
    void Update()
    {
        //Time.timeScale = 1.3f;

        //Random.seed = System.DateTime.Now.Millisecond;
        XSpeed = SpeedController.PlatformXSpeed;// +Random.Range(-0.2f, 0.2f);
        //Random.seed = System.DateTime.Now.Millisecond;
        YSpeed = SpeedController.PlatformYSpeed;// +Random.Range(-0.2f, 0.2f);
        CheckDirection();
        Move();

    }

    void Move()
    {
        if (!Fall)
        {
            if (MovingRight)
            {
                transform.Translate(XSpeed * Time.deltaTime, YSpeed * Time.deltaTime, 0f);
            }
            else if (!MovingRight)
            {
                transform.Translate(-XSpeed * Time.deltaTime, YSpeed * Time.deltaTime, 0f);
            }
        }
        else
        {
            transform.Translate(0f, -0.3f * Time.deltaTime, 0f);
            if (MovingRight)
            {
                transform.Translate(XSpeed * 0.5f * Time.deltaTime, YSpeed * Time.deltaTime, 0f);
            }
            else if (!MovingRight)
            {
                transform.Translate(-XSpeed * 0.5f * Time.deltaTime, YSpeed * Time.deltaTime, 0f);
            }
        }
    }

    void CheckDirection()
    {
        if (transform.position.x > RightLimit.x - 0.5f)
        {
            MovingRight = false;
        }

        else if (transform.position.x - 0.5f < LeftLimit.x)
        {
            MovingRight = true;
        }
    }

    public void SetPlatformMovement(bool A)
    {

        MovingRight = A;
    }

    public bool GetPlatformMovement()
    {

        return MovingRight;
    }

    void OnCollisionEnter2D()
    {
        Fall = true;
    }
}
