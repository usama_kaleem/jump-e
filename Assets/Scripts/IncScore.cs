﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IncScore : MonoBehaviour
{
    public Collider2D Col;

    public float SpeedFactor = 1f;   

    public Rigidbody2D Bob;
    public Vector2 Velocityy;

    public Slider Mana;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Velocityy = Bob.velocity;
        //Debug.Log("SPEED: " + SpeedController.PlatformXSpeed.ToString());
    }

    void OnTriggerEnter2D(Collider2D A)
    {
        if (A.gameObject.tag == "Player" && Velocityy.y > 0)
        {
            SpeedController.PlatformXSpeed *= SpeedFactor;
            Debug.Log("Inc Score");
            //ConnectBob.Score++;
            Mana.value += 8f;
        }
    }
}
