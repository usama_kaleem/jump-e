﻿using UnityEngine;
using System.Collections;

public class PauseFirstPlatform : MonoBehaviour
{
    MovePlatforms Change;
    public Transform Bob;
    // Use this for initialization
    void Start()
    {
        Change = GetComponent<MovePlatforms>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ConnectBob.Score == 0f)
        {
            Change.enabled = false;
            transform.position = new Vector3(Bob.transform.position.x, transform.position.y, transform.position.z);
        }

        else if (ConnectBob.Score > 0f)
        {
            //Change.enabled = true;
        }
    }
}
