﻿using UnityEngine;
using System.Collections;

public class StopperMoveUp : MonoBehaviour {

    public Transform Camera;
    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;
    // Use this for initialization
    void Start () {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
    }
	
	// Update is called once per frame
	void Update () {
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
        if (transform.position.y < LeftLimit.y-15f && ConnectBob.Score>0f)
        {
            transform.parent = Camera;
        }
	}
}
