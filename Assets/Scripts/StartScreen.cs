﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartScreen : MonoBehaviour
{
    public HELPME SaveMeText;

    public static bool tap = true;
    public static float Turns = 0f;
    public static bool SaveMeSeen = false;

    public Transform Dec1T, Dec2T, LogoT, UIT;

    public EndGame CameraHere;

    //public Transform PollSpawn;
    public GameObject Pole;

    public Rigidbody2D BobR;

    bool T = false;
    float t = 0f;

    public CamersFollow Camera;
    //public GameObject BobG;
    public NewJumpBob Bob;
    public AudioSource BobA, MenuA;
    public GameObject Dec1, Dec2, UI, Vig, Wires, Poll, LOGO, CREDITS, RainStartScreen;

    public Transform Target1, Target2, Target11, Target22;

    public Canvas ScoreC, PauseC;
    public Image ScoreImageHere; public Text ScoreTextHere;

    public static bool PLAYSOUND=true;
    GameObject PoleA;

    float TimeScale;

    bool isplaying = false;
    // Use this for initialization
    void Start()
    {
        CREDITS.SetActive(false);
        RainStartScreen.SetActive(true);
        SaveMeText.enabled = false;
        PauseC.enabled = false;
        Dec1T = Dec1.transform;
        Dec2T = Dec2.transform;
        LogoT = LOGO.transform;
        UIT = UI.transform;

        t = 0f;
        T = false;

        BobR.gravityScale = 0f;

        CameraHere.enabled = false; 

        //BobG.SetActive(false);
        Camera.enabled = false;
        ScoreC.enabled = false;
        Bob.enabled = false;
        //BobA.enabled = false;
        Wires.SetActive(false);
        //Poll.SetActive(false);
        //iTween.MoveTo(Dec1, new Vector3(Target1.position.x, Target1.position.y, Target1.position.z), 10f);

        iTween.MoveTo(Dec1, new Vector3(Target11.position.x, Target11.position.y, Target11.position.z), 200f);
        //iTween.MoveBy(Dec2, new Vector3(-15f, -50f, 0f), 200f);
        iTween.MoveTo(Dec2, new Vector3(Target22.position.x, Target22.position.y, Target22.position.z), 200f);
        BobA.volume = 0f;


    }

    // Update is called once per frame
    void Update()
    {
        StartTimer();
        CheckTimer();

        if(EndGame.isdead && PoleA)
        {
            Destroy(PoleA);
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            PLAY();
        }

        if(Input.GetKeyDown(KeyCode.Escape)/* && isplaying*/)
        {
            if (isplaying)
            {
                Time.timeScale = 0f;
                PauseC.enabled = true;
                isplaying = false;
                tap = false;
                ScoreC.enabled = false;
            }
            else if(!isplaying)
            {
                tap = true;
                Time.timeScale = 1.3f;
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }

    public void PLAY()
    {
        SaveMeText.enabled = true;
        isplaying = true;
        BobA.Play();

        RainStartScreen.SetActive(false);

        Camera.enabled = true;
        CamersFollow.CinematicOffset = -0.01f;
        // BobG.SetActive(true);
        // 
        //Wires.SetActive(true);
        //Poll.SetActive(true);

        //PoleA = Instantiate(Pole, Poll.transform.position, Pole.transform.rotation) as GameObject;

        ScoreC.enabled = true;
        ScoreTextHere.enabled = ScoreImageHere.enabled = false;
        iTween.MoveTo(Dec1, new Vector3(Target1.position.x, Target1.position.y, Target1.position.z), 8f);
        iTween.RotateAdd(Dec1, new Vector3(0f, 0f, 18f), 8f);
        iTween.ScaleBy(Dec1, new Vector3(1.7f, 1.7f, 1.7f), 8f);

        iTween.MoveTo(Dec2, new Vector3(Target2.position.x, Target2.position.y, Target2.position.z), 8f);
        iTween.RotateAdd(Dec2, new Vector3(0f, 0f, 18f), 8f);
        iTween.ScaleBy(Dec1, new Vector3(1.7f, 1.7f, 1.7f), 8f);

        iTween.MoveAdd(UI, new Vector3(0f, -1000f, 0f), 4f);
        iTween.MoveAdd(LOGO, new Vector3(0f, 1000f, 0f), 4f);

        iTween.ScaleBy(Vig, new Vector3(2f, 2f, 0f), 5f);
        //iTween.LoopType.loop(iTween.ScaleBy(Vig, new Vector3(2f, 2f, 0f), 5f));
        //iTween.ScaleBy()

        /*iTween.MoveTo(gameObject, iTween.Hash("x", 6, "time", 4, "loopType", "pingPong"
             , "delay", .4, "easeType", "easeInOutQuad"));*/    

        T = true;

        //Bob.enabled = true;
        BobA.volume = MenuA.volume;
        MenuA.volume = 0f;
        // MenuA.enabled = false;

    }

    public void Unpause()
    {
        Time.timeScale = 1.3f;
        PauseC.enabled = false;
        isplaying = true;
        tap = true;
        ScoreC.enabled = true;
    }

    public void BacktoMenu()
    {
        Debug.Log("K");
        //iTween.MoveTo(LOGO, new Vector3(LogoT.position.x, LogoT.position.y, LogoT.position.z), 2f);
    }

    public void Share()
    {
        Application.OpenURL("https://twitter.com/intent/tweet?text="/* + "I scored : " + ConnectBob.Score + */ + "Check this game out! ");
    }

    public void SOUND()
    {
//        PLAYSOUND = !PLAYSOUND;
       //BobA.enabled = MenuA.enabled = PLAYSOUND;
       // BobA.volume = 0f;

        if(MenuA.volume>0f)
        {
            MenuA.volume = 0f;
        }

        else
        {
            MenuA.volume = 1f;
        }
    }

    void CheckTimer()
    {
        if(t>0.4f)
        {
            //Debug.Log("Ten: " + t);
            //Bob.enabled = true;
            //BobR.gravityScale = 8f;
            T = false;
        }
    }

    void StartTimer()
    {
        if(T)
        {
            //Debug.Log("T: " + t);
            t += Time.deltaTime;
        }
    }

    public void OpenCredits()
    {
        UI.SetActive(false);
        CREDITS.SetActive(true);

        //iTween.MoveAdd(UI, new Vector3(0f, -1000f, 0f), 10f);
    }

    public void CloseCredits()
    {
        CREDITS.SetActive(false);
        UI.SetActive(true);
        //UI.transform.position = UIT.position;
        //iTween.MoveAdd(UI, new Vector3(0f, -1000f, 0f), 3f);
    }
}
