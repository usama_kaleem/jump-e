﻿using UnityEngine;
using System.Collections;

public class ShakeTiles : MonoBehaviour
{

    float ShakeAmount = 0.015f;
    public float ShakeLimit = 0.02f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (NewJumpBob.StartSlowMo && !EndGame.isdead)
        {
            float randNrX = Random.Range(ShakeAmount, -ShakeAmount);
            float randNrY = Random.Range(ShakeAmount, -ShakeAmount);
            float randNrZ = Random.Range(ShakeAmount, -ShakeAmount);
            transform.position += new Vector3(randNrX, randNrY, randNrZ);

            if (ShakeAmount < ShakeLimit)
                ShakeAmount += Time.deltaTime * 0.005f;
        }

        if (Input.GetMouseButtonUp(0))
            ShakeAmount = 0.007f;                   //Change Manually
    }
}
