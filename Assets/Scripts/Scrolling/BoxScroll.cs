﻿using UnityEngine;
using System.Collections;

public class BoxScroll : MonoBehaviour {
    public float LowerLimit, UpperLimit;
    public float Speed;
    bool Move = true;

    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;

    // Use this for initialization
    void Start()
    {
        Random.seed = System.DateTime.Now.Millisecond;
        //Speed = Random.Range(LowerLimit, UpperLimit) * 0.1f;
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
    }
	
	// Update is called once per frame
	void Update () {


        if(Move)
        transform.Translate(0f, -Speed * Time.deltaTime*0.5f, 0f);
        
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        if(transform.position.y < LeftLimit.y - 1f && Move)
        {
            transform.position = new Vector3(Random.Range(LeftLimit.x + 0.2f, RightLimit.x - 0.2f), RightLimit.y + 8f, transform.position.z);
            Random.seed = System.DateTime.Now.Millisecond; 
            Speed = Random.Range(LowerLimit, UpperLimit);
        }
	}
}
