﻿using UnityEngine;
using System.Collections;

public class Scroll1 : MonoBehaviour
{
    public float ScrollSpeed = 0.5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
            Vector2 Offset = new Vector3(0f, ScrollSpeed * Time.deltaTime * 0.2f, 0f);
            GetComponent<Renderer>().material.mainTextureOffset += Offset;﻿

    }
}
