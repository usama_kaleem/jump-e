﻿using UnityEngine;
using System.Collections;

public class SnapBackground : MonoBehaviour
{

    public Camera Cam;
    public bool IsRight = false;

    public Vector3 RightLimit, LeftLimit;
    public float Offset = 0f;

    // Use this for initialization
    void Start()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsRight)
            transform.position = new Vector3(LeftLimit.x + Offset, transform.position.y, transform.position.z);

        else if (IsRight)
            transform.position = new Vector3(RightLimit.x - Offset, transform.position.y, transform.position.z);
    }
}
