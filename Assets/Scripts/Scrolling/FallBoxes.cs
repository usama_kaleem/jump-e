﻿using UnityEngine;
using System.Collections;

public class FallBoxes : MonoBehaviour {

    BoxScroll BoxScrollScript;
    ShakeTiles ShakeTilesScript;

	// Use this for initialization
	void Start () {
        BoxScrollScript = GetComponent<BoxScroll>();
        ShakeTilesScript = GetComponent<ShakeTiles>();

    }
	
	// Update is called once per frame
	void Update () {
	    if(EndGame.isdead)
        {
            BoxScrollScript.enabled = false;
            ShakeTilesScript.enabled = false;
            transform.Translate(0f, -9.81f * Time.deltaTime, 0f);
            Destroy(this.gameObject, 5f);
        }
	}
}
