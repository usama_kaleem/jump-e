﻿using UnityEngine;
using System.Collections;

public class FLOATGIRL : MonoBehaviour {
    float times = 0f;
	// Use this for initialization
	void Start () {
        StartCoroutine(GoDown());

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator GoDown()
    {
        if (times < 5f)
        {
            iTween.MoveAdd(gameObject, new Vector3(0f, 0.2f, 0f), 1f);
            yield return new WaitForSeconds(1);
            StartCoroutine(GoUp());
        }
    }

    IEnumerator GoUp()
    {
        iTween.MoveAdd(gameObject, new Vector3(0f, -0.2f, 0f), 1f);
        yield return new WaitForSeconds(1);
        StartCoroutine(GoDown());

        times++;
    }
}
