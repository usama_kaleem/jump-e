﻿using UnityEngine;
using System.Collections;

public class RandomPlacer : MonoBehaviour
{

    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;

    // Use this for initialization
    void Start()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        Random.seed = System.DateTime.Now.Millisecond;
        //float H = Random.Range(20f, 60f);
        float W = Random.Range(LeftLimit.x + 2f, RightLimit.x - 2f);
        transform.position = new Vector3(W, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        CheckToRePosition();

        //iTween.ShakePosition(gameObject, { "y": 2, "LoopType": "PingPong"})
    }

    public void CheckToRePosition()
    {
        if (transform.position.y < LeftLimit.y/*-0.3f*/)
        {
            Random.seed = System.DateTime.Now.Millisecond;
            float H = Random.Range(20f, 50f);
            float W = Random.Range(LeftLimit.x + 2f, RightLimit.x - 2f);
            transform.position = new Vector3(W, transform.position.y + H-((transform.position.y + H)%4f), transform.position.z);
        }
    }
}
