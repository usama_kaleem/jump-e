﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public GameObject Rain, BackgroundQuad;
    Color BackgroundColor;
    public GameObject i1, i2, i3;

    /*
        bool T = false;
        float t = 0f;*/
    SpriteRenderer BobSR;
    Sprite BobS;

    public Transform CamT, JumpI; public GameObject PoleSpawner;//, StartNew;
    public GameObject Wires;
    Color OrigStarterColor;
    public GameObject P1, P2, P3, P4, P5, W1, W2, W3, W4, W5;
    Collider2D[] C1; Collider2D[] C2; Collider2D[] C3; Collider2D[] C4; Collider2D[] C5;

    Vector3 w1, w2, w3, w4, w5, p1, p2, p3, p4, p5, bob, starter, cam, stopper, jumpi;GameObject poll;

    float offsetWireStarter;

    public Transform BobT;

    public GameObject Starter, PolePrefab;
    public Transform StarterT;

    public Canvas HS;

    string HighScoreKey = "highscore";

    public GameObject Stopper;

    public NewJumpBob BobNJ;

    public Canvas DeathCanvas;
    public Canvas ScoreCanvas;
    public Text Score;
    public Text HighScore;

   public static bool GoOnce = false;

    public GameObject StartPlatform;
    public Transform Bob;
    Camera Cam;
    public Vector3 RightLimit, LeftLimit;
    public static bool isdead = false;

    GameObject P;
    public EndGame This;

    // Use this for initialization
    void Start()
    {
        BobSR = Bob.gameObject.GetComponent<SpriteRenderer>();
        BackgroundColor = BackgroundQuad.GetComponent<Renderer>().material.color;
        BobS = BobSR.sprite;

        /*
                t = 0f;
                T = false;*/
        jumpi = JumpI.position;

        p1 = P1.transform.position;
        p2 = P2.transform.position;
        p3 = P3.transform.position;
        p4 = P4.transform.position;
        p5 = P5.transform.position;
        /*
        w1 = W1.transform.position;
        w2 = W2.transform.position;
        w3 = W3.transform.position;
        w4 = W4.transform.position;
        w5 = W5.transform.position;
        */
        bob = Bob.position;

        starter = StarterT.position;

        cam = CamT.position;

        stopper = Stopper.transform.position;

        //poll.transform.position = PoleSpawner.transform.position;


        SpriteRenderer SR = Starter.GetComponent<SpriteRenderer>();
        OrigStarterColor = SR.material.color;

        C1 = P1.GetComponents<Collider2D>();
        C2 = P2.GetComponents<Collider2D>();
        C3 = P3.GetComponents<Collider2D>();
        C4 = P4.GetComponents<Collider2D>();
        C5 = P5.GetComponents<Collider2D>();


        StarterT = Starter.GetComponent<Transform>();
        offsetWireStarter = Wires.transform.position.y - StarterT.position.y;
        isdead = false;
        Cam = GetComponent<Camera>();
        DeathCanvas.enabled = false;
        GoOnce = false;
        HighScore.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        /*StartTimer();
        CheckTimer();*/


        SetCameraPoints();
        CheckDead();
        Dead();

        /*if(Input.GetKeyDown(KeyCode.Escape))
        {
            Bob.parent = null;
            ReplayLevel();
        }*/

        //Debug.Log("isDead: " + isdead);
    }

    void SetCameraPoints()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
    }

    void Dead()
    {
        if (isdead)
        {/*
            if (BobHealth.LivesRemaining > 0f)
            {
                StartPlatform.SetActive(true);
            }
            else
            {*/

            NewJumpBob.StartSloTimer = false;
            NewJumpBob.StartSlowMo = false;

            Bob.transform.parent = null;
                Stopper.transform.parent = null;
                DeathCanvas.enabled = true;
                CamersFollow.MoveSpeed = 0.70f;
                CamersFollow.CinematicOffset = 0.17f;
                ScoreCanvas.enabled = false;
                Score.text = /*"Score: " + */ConnectBob.Score.ToString();
                //HighScore.enabled = true;
                if (!GoOnce) { StoreHighscore(ConnectBob.Score); BobS = null; GoOnce = true; }
            //}
        }
        else
        {
            DeathCanvas.enabled = false;
        }
    }

    void CheckDead()
    {
        if (Bob.position.y < LeftLimit.y)
        {
            isdead = true;
        }
    }

    void StoreHighscore(float newHighscore)
    {
        //Debug.Log("NEW Score: " + newHighscore);
        float oldHighscore = PlayerPrefs.GetFloat(HighScoreKey, 0);
        //Debug.Log("OLDHIGHSCORE: " + oldHighscore.ToString());
        if (newHighscore >= oldHighscore)
        {
            PlayerPrefs.SetFloat(HighScoreKey, newHighscore); PlayerPrefs.Save();
            HighScore.text = "NEW HIGHSCORE!";
            //Debug.Log("NEWHIGHSCORE: " + (PlayerPrefs.GetFloat(HighScoreKey)).ToString()); 
            //Debug.Log("IF");
        }
        else if (newHighscore < oldHighscore)
        {
            //Debug.Log("ELSE");
            HighScore.text = "HIGHSCORE : " + oldHighscore.ToString();
            //if (PlayGameServices.isSignedIn()) { Leaderboarder.SubmitScorer(); }
        }
    }

    public void ReplayLevel()
    {
        CallRain();

        RespawnWire.ScoreA = 9f;
        RespawnWire.ScoreB = 13f;
        RespawnWire.ScoreJ = 2f;
        RespawnWire.IsJumper = false;

        if(PlayerPrefs.GetFloat("TutorialOK", 0f)==2f)
        {
            if(i1)
            i1.SetActive(false);

            if(i2)
            i2.SetActive(false);

            if (i3)
                i3.SetActive(false);
        }

        if(PlayerPrefs.GetFloat("TutorialOK", 0f) < 2f)
        {
            PlayerPrefs.SetFloat("TutorialOK", 0f);
        }

        This.enabled = false;
        DeathCanvas.enabled = false;

        BobNJ.enabled = true;
        
        //HS.enabled = false;
        //Application.LoadLevel(Application.loadedLevel);

        StartScreen.Turns++;

        SpeedController.PlatformXSpeed = 2.5f;

        Starter.SetActive(true);

        SpriteRenderer SR = Starter.GetComponent<SpriteRenderer>();
        SR.material.color = OrigStarterColor;

        CamersFollow.CinematicOffset = 0.04f;
        //CamersFollow.MoveSpeed = 0.15f;


        foreach (Collider2D c in C1)
        {
            c.enabled = true;
        }

        foreach (Collider2D c in C2)
        {
            c.enabled = true;
        }

        foreach (Collider2D c in C3)
        {
            c.enabled = true;
        }

        foreach (Collider2D c in C4)
        {
            c.enabled = true;
        }

        foreach (Collider2D c in C5)
        {
            c.enabled = true;
        }

        if(P)
        Destroy(P);
        //P = Instantiate(PolePrefab, PoleSpawner.transform.position, PoleSpawner.transform.rotation) as GameObject;

        P1.transform.position = p1;// +new Vector3(0f, -250f,0f);
        P2.transform.position = p2;// + new Vector3(0f, -250f, 0f);
        P3.transform.position = p3;// + new Vector3(0f, -250f, 0f);
        P4.transform.position = p4;// + new Vector3(0f, -250f, 0f);
        P5.transform.position = p5;// + new Vector3(0f, -250f, 0f);

        JumpI.position = jumpi;// + new Vector3(0f, -250f, 0f); ;
                               /*
                               W1.transform.position = new Vector3(W1.transform.position.x, P1.transform.position.y, W1.transform.position.z); //w1;
                               W2.transform.position = new Vector3(W2.transform.position.x, P2.transform.position.y, W2.transform.position.z);//w2;
                               W3.transform.position = new Vector3(W3.transform.position.x, P3.transform.position.y, W3.transform.position.z);//w3;
                               W4.transform.position = new Vector3(W4.transform.position.x, P4.transform.position.y, W4.transform.position.z);//w4;
                               W5.transform.position = new Vector3(W5.transform.position.x, P5.transform.position.y, W5.transform.position.z);//w5;*/


        //DELAY HERE
        BobT.position = bob;// + new Vector3(0f, -250f, 0f);

        StarterT.position = starter;// + new Vector3(0f, -250f, 0f);

        NewJumpBob.BlastOnce = false;

        Cam.transform.position = bob + new Vector3(0f, /*-245f*/0f, -10f); ;// new Vector3(Cam.transform.position.x, Bob.position.y + 5f, Cam.transform.position.z) + new Vector3(0f, -250f, 0f);

        Stopper.transform.position = stopper;// + new Vector3(0f, -250f, 0f);

        //StarterT.position = new Vector3(StarterT.position.x, RightLimit.y, StarterT.position.z);
        //Wires.transform.position = new Vector3(Wires.transform.position.x, StarterT.position.y + offsetWireStarter, Wires.transform.position.z);
        //BobT.position = new Vector3(StarterT.position.x, StarterT.position.y + 0.2f, StarterT.position.z);
        
        NewJumpBob.CanJump = true;

        //Stopper.transform.position = new Vector3(Stopper.transform.position.x, Stopper.transform.position.y - 15f, Stopper.transform.position.z);
        //Stopper.transform.parent = this.transform;
        ScoreCanvas.enabled = true;
        ConnectBob.Score = 0f;

        Collider2D[] StarterCols = Starter.GetComponents<Collider2D>();
        foreach (Collider2D c in StarterCols)
        {
            c.enabled = true;
        }

        if(StartScreen.Turns<2f)
        {
            ReplayLevel();
        }

        //Debug.Log("TURNS: " + StartScreen.Turns);

        CamersFollow.MoveSpeed = 0.15f;
        GoOnce = false;
        isdead = false;
        SpeedController.PlatformXSpeed = 1.6f;
        /*if (NewJumpBob.SlowOnce)
        {
            NewJumpBob.MouseDown = false;
            NewJumpBob.StartSlowMo = false;
            NewJumpBob.SlowOnce = false;
        }
        */

        BackgroundQuad.GetComponent<Renderer>().material.color = BackgroundColor;


    }

    void CallRain()
    {
        Random.seed = System.DateTime.Now.Millisecond;

        if(Random.Range(0f, 100f) > 60f)
        {
            Rain.SetActive(true);
            //Debug.Log("Rain is: ON");
        }

        else
        {
            Rain.SetActive(false);
            //Debug.Log("Rain is: OFF");
        }
    }

}
