﻿using UnityEngine;
using System.Collections;

public class RespawnWire : MonoBehaviour
{
    public GameObject Jumper;
    public bool IsSpecial = false; public static bool IsJumper = false;
    Sprite OriginalSprite;
    public Sprite SpecialSprite, JumperSprite;

    //public static bool IsJump = false;
    SpriteRenderer SR;

    public static float ScoreA, ScoreB, ScoreJ;

    public float RespawnHeight = 2f;
    public GameObject PlatformOfThisWire;
    //public Collider2D WireCollider;
    Collider2D[] arr;

    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;

    public static float previousX;
    public static bool previousRightMovement;

    public MovePlatforms Change;

    void Awake()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        previousX = LeftLimit.x;
        previousRightMovement = false;
    }

    void Start()
    {
        if(Jumper)
        Jumper.SetActive(false);
        ScoreJ = 2f;
        ScoreA = 9f;
        ScoreB = 13f;
        SR = GetComponent<SpriteRenderer>();
        OriginalSprite = SR.sprite;
        arr = PlatformOfThisWire.GetComponents<Collider2D>();
        CheckToRePosition();
        Change = PlatformOfThisWire.GetComponent<MovePlatforms>();
    }

    // Update is called once per frame
    void Update()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        //Debug.Log("ISJUMPER: " + IsJumper);

        CheckToRePosition();

        /*if(ConnectBob.Score==0f)
        {
            Change.enabled = false;
        }

        else if(ConnectBob.Score>0f)
        {
            Change.enabled = true;
        }*/
    }

    void CheckToRePosition()    //PlatformOfThisWire should be used here
    {
        if (transform.position.y < LeftLimit.y-0.3f)
        {

            if (Jumper.activeInHierarchy)
            { Jumper.SetActive(false); IsJumper = false; }

                //CHECK FOR SPECIAL
                if (ConnectBob.Score>ScoreA && ConnectBob.Score<ScoreB)
            {
                Debug.Log("Is Special Platform");
                IsSpecial = true;
                SR.sprite = SpecialSprite;
            }

            if (ConnectBob.Score > ScoreJ && !IsJumper)
            {
                //Debug.Log("Is jumping Platform");

                IsJumper = true;

                if(JumperSprite)
                SR.sprite = JumperSprite;

                if(Jumper)
                Jumper.SetActive(true);

                ScoreJ += ConnectBob.Score + 4f + Random.Range(7f, 10f);
                //Debug.Log("Jumping Score: " + ScoreJ);
            }

            else if(/*(*/ConnectBob.Score>ScoreB && IsSpecial)// || IsJumper)
            {
                Debug.Log("Is NOT Special Platform");

                SR.sprite = OriginalSprite;
                IsSpecial = false;

                ScoreA = ConnectBob.Score + Random.Range(5f, 20f);
                ScoreB = ScoreA + Random.Range(3f, 5f);

//                IsJumper = false;
            }

            



            transform.position = new Vector3(transform.position.x, transform.position.y + 10f, transform.position.z);

            //Random.seed = System.DateTime.Now.Millisecond;
            Random.seed = (int)System.DateTime.Now.Ticks;
            float W = Random.Range(LeftLimit.x, RightLimit.x) % 2f;
            //Debug.Log("W1: " + W.ToString());
            while (Mathf.Abs(previousX-W)<3f)
            {
                //Debug.Log("ABS: " + Mathf.Abs(W - previousX).ToString());
                //Random.seed = System.DateTime.Now.Millisecond;
                Random.seed = (int)System.DateTime.Now.Ticks;
                W = Random.Range(LeftLimit.x, RightLimit.x) % 3f;
            }

            previousX = W;
            //Debug.Log("PREVX: " + previousX.ToString());
            PlatformOfThisWire.transform.position = new Vector3(W, transform.position.y, transform.position.z);
            //Debug.Log("W2: " + W.ToString());

            foreach (Collider2D c in arr)
                c.enabled = true;

            if (previousRightMovement)
            {
                //set right movement
                Change.MovingRight = false;
                previousRightMovement = false;
                //Debug.Log("MovingL");

                Change.SetPlatformMovement(false);
            }

            else if (!previousRightMovement)
            {
                Change.MovingRight = true;
                previousRightMovement = true;
                //Debug.Log("MovingR");

                Change.SetPlatformMovement(true);
            }
        }

        if((transform.position.y>RightLimit.y+0.5f)&&(CamersFollow.interpVelocity>25.9f))
        {
           // Debug.Log("Should show wire");

            transform.position = new Vector3(transform.position.x, transform.position.y - 10f, transform.position.z);
        }
    }
}
