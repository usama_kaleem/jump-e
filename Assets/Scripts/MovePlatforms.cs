﻿using UnityEngine;
using System.Collections;

public class MovePlatforms : MonoBehaviour
{
    Sprite OrigSp;
    public float XSpeed;
    public float YSpeed;
    public bool MovingRight;

    public Camera Cam;

    public Vector3 RightLimit, LeftLimit;

    public static float previousX;

    public RespawnWire ThisPlatform;

    void Awake()
    {
        RightLimit = Cam.ScreenToWorldPoint(new Vector3(Cam.pixelWidth, Cam.pixelHeight, 0f));
        LeftLimit = Cam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
        previousX = LeftLimit.x;
    }

    // Use this for initialization
    void Start()
    {
        OrigSp = GetComponent<SpriteRenderer>().sprite;
        XSpeed = SpeedController.PlatformXSpeed;
        YSpeed = SpeedController.PlatformYSpeed;
        //MovingRight = true;           //Default
        
    }

    // Update is called once per frame
    void Update()
    {
        //Time.timeScale = 1.3f;

        //Random.seed = System.DateTime.Now.Millisecond;

        if(!ThisPlatform.IsSpecial)
        XSpeed = SpeedController.PlatformXSpeed;// + Random.Range(-0.2f, 0.2f);
        /*Random.seed = System.DateTime.Now.Millisecond;
        YSpeed = SpeedController.PlatformYSpeed + Random.Range(-0.2f, 0.2f);*/

        else if(ThisPlatform.IsSpecial)
        {
            XSpeed = SpeedController.PlatformXSpeed * 0.2f;
        }
        CheckDirection();
        Move();

    }

    void Move()
    {
        if (MovingRight)
        {
            transform.Translate(XSpeed * Time.deltaTime, YSpeed * Time.deltaTime, 0f);
        }
        else if (!MovingRight)
        {
            transform.Translate(-XSpeed * Time.deltaTime, YSpeed * Time.deltaTime, 0f);
        }
    }

    void CheckDirection()
    {
        if (transform.position.x > RightLimit.x - 0.9f)
        {
            MovingRight = false;
        }

        else if (transform.position.x - 0.9f < LeftLimit.x)
        {
            MovingRight = true;
        }
    }

    public void SetPlatformMovement(bool A)
    {

        MovingRight = A;
    }

    public bool GetPlatformMovement()
    {

        return MovingRight;
    }

    void OnTriggerEnter2D(Collider2D A)
    {
        if(A.gameObject.tag=="Player")
        {
            if(ThisPlatform.IsSpecial)
            ThisPlatform.IsSpecial = false;
            GetComponent<SpriteRenderer>().sprite = OrigSp;
        }
    }
}
