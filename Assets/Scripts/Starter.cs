﻿using UnityEngine;
using System.Collections;

public class Starter : MonoBehaviour
{
    public Transform Bob;
    public Transform Camera;    //CAN BE USED TO REPLAY IF MULTIPLE HEALTHS

    void Start()
    {

    }

    void Update()
    {
        if(Bob.position.y > transform.position.y + 2f)
        {
            SpriteRenderer SR = GetComponent<SpriteRenderer>();
            SR.material.color = new Color(SR.material.color.r, SR.material.color.g, SR.material.color.b, 0f);
            //this.gameObject.SetActive(false);
            Collider2D[] StarterCols = GetComponents<Collider2D>();
            foreach (Collider2D c in StarterCols)
            {
                c.enabled = false;
            }
        }
    }
}
