﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour
{
    public NewJumpBob Bob;
    public static float TutorialComplete;
    public static float Step;
    public GameObject i1, i2, i3;
    public bool a, b, c;

    // Use this for initialization
    void Start()
    {
        //PlayerPrefs.SetFloat("TutorialOK", 0f);
        if (PlayerPrefs.GetFloat("TutorialOK", 0f) < 2f)
        {
            PlayerPrefs.SetFloat("TutorialOK", 0f);
            if (i1 && i2 && i3)
            {
                //Wait for 1 sec
                i1.SetActive(true);
                i2.SetActive(false);
                i3.SetActive(false);
            }
        }

        a = b = c = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetFloat("TutorialOK", 0f) < 2f)
        {
            //Tap To Jump
            if (Input.GetMouseButtonDown(0) && Bob.enabled==true)
            {
                if (i1/* && a*/)
                {
                    //WAIT FOR 1 SECOND
                    i1.SetActive(false); i2.SetActive(true); i3.SetActive(false); Debug.Log("Disable 'TAP TO JUMP'"); PlayerPrefs.SetFloat("TutorialOK", 1f);// a = true;
                }
            }
        }
    }
}
