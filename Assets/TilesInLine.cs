﻿using UnityEngine;
using System.Collections;

public class TilesInLine : MonoBehaviour {

    public Transform P1, P2, P3, P4, P5;
    MovePlatforms p1, p2, p3, p4, p5;
    public float T = 0f;
    public bool GoTimer = false;

	// Use this for initialization
	void Start () {
        p1 = P1.gameObject.GetComponent<MovePlatforms>();
        p2 = P2.gameObject.GetComponent<MovePlatforms>();
        p3 = P3.gameObject.GetComponent<MovePlatforms>();
        p4 = P4.gameObject.GetComponent<MovePlatforms>();
        p5 = P5.gameObject.GetComponent<MovePlatforms>();
    }
	
	// Update is called once per frame
	void Update () {
	    if(GoTimer)
        {
            T += Time.deltaTime;
        }
	}

    void OnTriggerEnter2D(Collider2D A)
    {
        GoTimer = true;

        /*p1.enabled = p2.enabled = p3.enabled = p4.enabled = p5.enabled = false;
        Debug.Log("Disabled movement");*/

    }
}
