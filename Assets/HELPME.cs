﻿using UnityEngine;
using System.Collections;

public class HELPME : MonoBehaviour {

    public bool PressedOnce=false;
	// Use this for initialization
	void Start () {
      //  Debug.Log("GO");
	}
	
	// Update is called once per frame
	void Update () {
	    if(gameObject.activeInHierarchy)
        {
            StartCoroutine(HELPMEPLS());
        }
	}

    public IEnumerator HELPMEPLS()
    {
        if (!PressedOnce)
        {
            PressedOnce = true;
            yield return new WaitForSeconds(1);
            iTween.MoveAdd(gameObject, new Vector3(0f, 1.5f, 0f), 1f);
            iTween.ScaleBy(gameObject, new Vector3(7f, 7f, 7f), 1f);
        }
    }
}
