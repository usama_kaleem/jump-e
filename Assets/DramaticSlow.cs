﻿using UnityEngine;
using System.Collections;

public class DramaticSlow : MonoBehaviour {

    public bool SlowOnce = false;
    public Rigidbody2D Bob;
    float time;

	// Use this for initialization
	void Start () {
        time = Time.timeScale;
	}
	
	// Update is called once per frame
	void Update () {
	    if((Bob.velocity.y<0.5f) && !NewJumpBob.CanJump && !SlowOnce)
        {
            //Debug.Log("Slo Time");
            //Bob.gravityScale = 1f;
            SlowOnce = true;
        }

        else if(Bob.gravityScale>1f &&Bob.gravityScale<8f)
        {
            //Bob.gravityScale += 0.1f;
            Debug.Log("Gravity Speed Up: " + Bob.gravityScale);
        }
	}

    void OnCollisionEnter2D(Collision2D A)
    {
        if(A.gameObject.tag=="Player")
        {
            //Debug.Log("Reset Time");
            Bob.gravityScale = 8f;
            SlowOnce = false;
        }
    }
}
