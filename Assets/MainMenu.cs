﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    public Image SCORE;
    public GameObject Bob;

    void Start()
    {
        SCORE.enabled = false;
        Bob.SetActive(false);
    }

    public void PressPlay()
    {
        SCORE.enabled = true;
        Bob.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
